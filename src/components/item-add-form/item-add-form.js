import React, { Component } from "react";
import "./item-add-form.css";

class ItemAddForm extends Component {
  state = {
    value: ""
  };

  onLabelChange = event => {
    this.setState({ value: event.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.onItemAdded(this.state.value);
    this.setState({
      value: ''
    })
  };

  render() {
    return (
      <form className="item-add-form d-flex" onSubmit={this.onSubmit}>
        <input type="text" value={this.state.value} className="form-control" onChange={this.onLabelChange} />
        <button className="btn btn-outline-secondary">Add Item</button>
      </form>
    );
  }
}

export default ItemAddForm;
